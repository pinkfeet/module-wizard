<?php
/*
Copyright (c) 2016, Kristián Feldsam, FELDSAM s.r.o. <info@feldsam.cz>
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ISPConfig nor the names of its contributors
      may be used to endorse or promote products derived from this software without
      specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

	Form Definition

	Tabledefinition

	Datatypes:
	- INTEGER (Forces the input to Int)
	- DOUBLE
	- CURRENCY (Formats the values to currency notation)
	- VARCHAR (no format check, maxlength: 255)
	- TEXT (no format check)
	- DATE (Dateformat, automatic conversion to timestamps)

	Formtype:
	- TEXT (Textfield)
	- TEXTAREA (Textarea)
	- PASSWORD (Password textfield, input is not shown when edited)
	- SELECT (Select option field)
	- RADIO
	- CHECKBOX
	- CHECKBOXARRAY
	- FILE

	VALUE:
	- Wert oder Array

	Hint:
	The ID field of the database table is not part of the datafield definition.
	The ID field must be always auto incement (int or bigint).

	Search:
	- searchable = 1 or searchable = 2 include the field in the search
	- searchable = 1: this field will be the title of the search result
	- searchable = 2: this field will be included in the description of the search result


*/

$form["title"]    = "Wizard template";
$form["description"]  = "";
$form["name"]    = "template";
$form["action"]   = "template_edit.php";
$form["db_table"]  = "wizard_template";
$form["db_table_idx"] = "template_id";
$form["db_history"]  = "yes";
$form["tab_default"] = "site";
$form["list_default"] = "template_list.php";
$form["auth"]   = 'yes'; // yes / no

$form["auth_preset"]["userid"]  = 0; // 0 = id of the user, > 0 id must match with id of current user
$form["auth_preset"]["groupid"] = 0; // 0 = default groupid of the user, > 0 id must match with groupid of current user
$form["auth_preset"]["perm_user"] = 'riud'; //r = read, i = insert, u = update, d = delete
$form["auth_preset"]["perm_group"] = 'riud'; //r = read, i = insert, u = update, d = delete
$form["auth_preset"]["perm_other"] = ''; //r = read, i = insert, u = update, d = delete

$form["tabs"]['site'] = array(
	'title'  => "Web",
	'width'  => 100,
	'template'  => "templates/template_site.htm",
	'fields' => load_foreing_form('site', '../sites/form/web_vhost_domain.tform.php'),
);

$form["tabs"]['database'] = array(
	'title'  => "Database",
	'width'  => 100,
	'template'  => "templates/template_database.htm",
	'fields' => load_foreing_form('database', '../sites/form/database.tform.php'),
);

$form["tabs"]['ftp'] = array(
	'title'  => "FTP",
	'width'  => 100,
	'template'  => "templates/template_ftp.htm",
	'fields' => load_foreing_form('ftp', '../sites/form/ftp_user.tform.php'),
);

$form["tabs"]['ssh'] = array(
	'title'  => "SSH",
	'width'  => 100,
	'template'  => "templates/template_ssh.htm",
	'fields' => load_foreing_form('ssh', '../sites/form/shell_user.tform.php'),
);

$form["tabs"]['mail'] = array(
	'title'  => "Mail",
	'width'  => 100,
	'template'  => "templates/template_mail.htm",
	'fields' => load_foreing_form('mail', '../mail/form/mail_domain.tform.php'),
);

$form["tabs"]['dns'] = array(
	'title'  => "DNS",
	'width'  => 100,
	'template'  => "templates/template_dns.htm",
	'fields' => array(
		'dns_server_id' => array (
			'datatype' => 'INTEGER',
			'formtype' => 'SELECT',
			'default' => '',
			'datasource' => array (  'type' => 'SQL',
				'querystring' => 'SELECT server_id, server_name FROM server WHERE mirror_server_id = 0 AND {AUTHSQL} AND dns_server = 1 ORDER BY server_name',
				'keyfield'=> 'server_id',
				'valuefield'=> 'server_name',
			),
			'value'  => '',
		),
	),
);

function load_foreing_form($type, $path)
{
	global $app;
	
	require $path;
	
	switch($type)
	{
		case 'site':
			$new_form = $form['tabs']['domain']['fields'] + $form['tabs']['redirect']['fields'] + $form['tabs']['advanced']['fields'];
	
			// unset fields
			unset($new_form['domain']);
			unset($new_form['type']);
			unset($new_form['vhost_type']);
			unset($new_form['ssl']);
			unset($new_form['ssl_letsencrypt']);
			unset($new_form['web_folder']);
			unset($new_form['parent_domain_id']);
			unset($new_form['active']);
			unset($new_form['redirect_type']);
			unset($new_form['redirect_path']);
			unset($new_form['rewrite_rules']);
			unset($new_form['document_root']);
			unset($new_form['system_user']);
			unset($new_form['system_group']);
			unset($new_form['php_open_basedir']);
			unset($new_form['proxy_directives']);
			unset($new_form['added_date']);
			unset($new_form['added_by']);
			unset($new_form['enable_pagespeed']);
			
			// rename server id
			$new_form['web_server_id'] = $new_form['server_id'];
			unset($new_form['server_id']);
			
			// add template name field
			$new_form['template_name'] = array (
				'datatype' => 'VARCHAR',
				'formtype' => 'TEXT',
				'validators' => array (  0 => array ( 'type' => 'NOTEMPTY',
						'errmsg'=> 'template_name_error_empty'),
				),
				'default' => '',
				'value'  => '',
				'width'  => '40',
				'maxlength' => '255',
			);
			break;
		case 'database':
			$new_form = $form['tabs']['database']['fields'];
		
			// unset fields
			unset($new_form['parent_domain_id']);
			unset($new_form['type']);
			unset($new_form['database_name']);
			unset($new_form['database_name_prefix']);
			unset($new_form['database_user_id']);
			unset($new_form['database_ro_user_id']);
			unset($new_form['active']);
			
			// rename fields
			$new_form['database_server_id'] = $new_form['server_id'];
			unset($new_form['server_id']);
			$new_form['database_remote_access'] = $new_form['remote_access'];
			unset($new_form['remote_access']);
			$new_form['database_remote_ips'] = $new_form['remote_ips'];
			unset($new_form['remote_ips']);
			
			break;
		case 'ftp':
			$new_form = $form['tabs']['advanced']['fields'];
			
			// unset fields
			unset($new_form['uid']);
			unset($new_form['gid']);
			unset($new_form['dir']);
			unset($new_form['expires']);
		
			break;
		case 'ssh':
			$new_form = $form['tabs']['advanced']['fields'];
			
			// unset fields
			unset($new_form['puser']);
			unset($new_form['pgroup']);
			unset($new_form['dir']);
		
			// add chroot
			$new_form['shell_chroot'] = array (
				'datatype' => 'VARCHAR',
				'formtype' => 'SELECT',
				'default' => '',
				'value'  => array('no' => 'None', 'jailkit' => 'Jailkit')
			);
		
			break;
		case 'mail':
			$new_form = $form['tabs']['domain']['fields'];
		
			// unset fields
			unset($new_form['domain']);
			unset($new_form['active']);
			unset($new_form['dkim']);
			unset($new_form['dkim_selector']);
			unset($new_form['dkim_private']);
			unset($new_form['dkim_public']);
		
			// rename server id
			$new_form['mail_server_id'] = $new_form['server_id'];
			unset($new_form['server_id']);
			
			// add policy id
			$new_form['policy'] = array(
				'datatype' => 'INTEGER',
				'formtype' => 'SELECT',
				'default' => '',
				'datasource' => array (  'type' => 'SQL',
					'querystring' => 'SELECT id, policy_name FROM spamfilter_policy WHERE {AUTHSQL} ORDER BY policy_name',
					'keyfield'=> 'id',
					'valuefield'=> 'policy_name'
				),
				'value'  => '',	
			);
			
			// add mailbox options
			$new_form['mailbox_quota'] = array (
				'datatype' => 'VARCHAR',
				'formtype' => 'TEXT',
				'validators' => array (  1 => array ( 'type' => 'ISINT',
						'errmsg'=> 'quota_error_isint'),
					0 => array ( 'type' => 'REGEX',
						'regex' => '/^([0-9]{1,})$/',
						'errmsg'=> 'quota_error_value'),
				),
				'default' => '-1',
				'value'  => '',
				'width'  => '30',
				'maxlength' => '255'
			);
			$new_form['enablesmtp'] = array (
				'datatype' => 'VARCHAR',
				'formtype' => 'CHECKBOX',
				'default' => 'y',
				'value'  => array(1 => 'y', 0 => 'n')
			);
			$new_form['enableimap'] = array (
				'datatype' => 'VARCHAR',
				'formtype' => 'CHECKBOX',
				'default' => 'y',
				'value'  => array(1 => 'y', 0 => 'n')
			);
			$new_form['enablepop3'] = array (
				'datatype' => 'VARCHAR',
				'formtype' => 'CHECKBOX',
				'default' => 'y',
				'value'  => array(1 => 'y', 0 => 'n')
			);
			break;
	}
	
	return $new_form;
}