# ISPConfig Wizard module

This module allows you to create DNS Zone, Site, Database User, Database, FTP User, SSH User, Mail Domain and Mailboxes in one form. You just define domain name and using checkboxes decide which services you want to create. Services are created using user defined templates.

This module is compatible with ISPConfig version 3.1

## Installation

This module have to be installed on master node in multiserver setup.

- Download this repo as archive, unpack it to `/usr/local/ispconfig/interface/web/` and rename forlder to `wizard` OR  
clone repo using git `git clone https://git.ispconfig.org/ispconfig/module-wizard.git /usr/local/ispconfig/interface/web/wizard`
- Create DB table provided in db.sql file `mysql -u root -p < db.sql`
- Enable module in user interface System -> CP Users -> Admin user -> Check "wizard" and save.
  - If it doesn't work, enable module manually by editing admin user in DB table `sys_user` column `modules`
  
## Planned features

- Export results as PDF
- Email results as HTML/PDF

## Screenshots

![New service](https://git.ispconfig.org/ispconfig/module-wizard/raw/master/readme_images/new_service.png)

![Template](https://git.ispconfig.org/ispconfig/module-wizard/raw/master/readme_images/template.png)
